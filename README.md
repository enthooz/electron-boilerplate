# Electron Boilerplate

## Configuration

### `package.json`

:information_source: See `electron-builder` [Common Configuration](https://www.electron.build/configuration/configuration), [Any macOS Target](https://www.electron.build/configuration/mac), [Any Linux Target](https://www.electron.build/configuration/linux), and ...

```json
  "build": {
    "appId": "com.enthooz.electron-boilerplate",
    "productName": "Electron Boilerplate",
    "mac": {
      "category": "public.app-category.reference",
      "target": "dmg",
      "icon": "assets/icons/awesome_emoji.icns",
      "darkModeSupport": false,
      "type": "distribution"
    },
    "linux": {
      "target": "deb",
      "maintainer": "gitlab.com/enthooz",
      "icon": "assets/icons/png/",
      "category": "Education"
    }
```

- `build.productName` allows spaces and other special characters not allowed in `name`.
- For `build.mac.category`, see [LSApplicationCategoryType](https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/LaunchServicesKeys.html#//apple_ref/doc/uid/TP40009250-SW8).
- `build.mac.type` is either "distribution" or "development".
- `build.linux.target` has many options including "deb" and "rpm".  See [Any Linux Target](https://www.electron.build/configuration/linux).
- `build.linux.icon` 
- For `build.linux.category` , see [Main Categories](https://specifications.freedesktop.org/menu-spec/latest/apa.html#main-category-registry) (freedesktop.org).


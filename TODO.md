- [ ] Install [eslint](https://eslint.org/) to ensure clean coding styles.
- [ ] Consider using [esdoc](https://esdoc.org) to generate documentation.
- [ ] Consider using [commitizen](https://github.com/commitizen/cz-cli) to ensure commit messages are consistent.
- [ ] Consider using [conventional-changelog-cli](https://www.npmjs.com/package/conventional-changelog-cli) to generate changelogs.
- [ ] Consider using [husky](https://www.npmjs.com/package/husky) for pre-commit hooks, for example.

## Reference

- [Node.js Documentation](https://nodejs.org/api/modules.html#modules_modules) (link is to `Modules` page)
- [How to setup npm project for you and your team with automated formatting, linting, testing and auto-generated documentation](https://medium.com/@drag13dev/https-medium-com-drag13dev-how-to-setup-npm-project-for-you-and-your-team-a7de38e5a2f7)
- Electron [boiler plate repos](https://github.com/search?utf8=%E2%9C%93&q=electron%20starter&type=Repositories) in Github
- Linting
  - [Eslint](https://eslint.org)
  - [JavaScript Standard Style](https://github.com/standard/standard#why-should-i-use-javascript-standard-style)
- Testing
  - [Jest](https://jestjs.io/docs/en/getting-started)
  - [Spectron](https://electronjs.org/spectron)
  - [Jest + Selenium WebDriver](https://medium.com/@mathieux51/jest-selenium-webdriver-e25604969c6)
- [iConvert Icons](https://iconverticons.com/online/)
- Supporting a custom protocol e.g. `kultr://verify/ab6c6d7bad/`:
  - [Electron app with custom protocol](https://glebbahmutov.com/blog/electron-app-with-custom-protocol/) — Uses [electron-builder](https://github.com/electron-userland/electron-builder).
- Updating application:
  - [Updating Applications](https://electronjs.org/docs/tutorial/updates) (Electron documentation)
- Installation:
  - [Application Distribution](https://electronjs.org/docs/tutorial/application-distribution)
const Application = require('spectron').Application
const electronPath = require('electron') // Require Electron from the binaries included in node_modules.
const path = require('path')

describe('Application launch', () => {
  jest.setTimeout(10000)

  let app

  beforeEach(() => {
    app = new Application({
      path: electronPath,
      args: [path.join(__dirname, '..')]
    });
    return app.start()
  })

  afterEach(() => {
    if (app && app.isRunning()) {
      return app.stop()
    }
  })

  test('shows an initial window', function () {
    return app.client.getWindowCount().then(count => {
      expect(count).toBe(1)
    })
  })
})
